package com.ciber.bagscan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onAlpha(View view) {
        // Create an Intent to start the second activity
        Intent secondScreen = new Intent(this, Main2Activity.class);

        // Start the new activity.
        startActivity(secondScreen);
    }

    public void onBeta(View view) {
        TextView glarphView = (TextView) findViewById(R.id.glarphText);

        java.util.GregorianCalendar g = new GregorianCalendar();
        String x;
        x = g.getTime().toString();

        glarphView.setText(x);
    }

    public void onGamma(View view) {
        TextView glarphView = (TextView) findViewById(R.id.glarphText);
        glarphView.setText("Glarph!");

    }
}
